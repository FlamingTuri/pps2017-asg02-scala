package u05lab.solution

/** Implement trait Functions with an object FunctionsImpl such that the code
  * in TryFunctions works correctly. To apply DRY principle at the best,
  * note the three methods in Functions do something similar.
  * Use the following approach:
  * - find three implementations of Combiner that tell (for sum,concat and max) how
  * to combine two elements, and what to return when the input list is empty
  * - implement in FunctionsImpl a single method combiner that, other than
  * the collection of A, takes a Combiner as input
  * - implement the three methods by simply calling combiner
  *
  * When all works, note we completely avoided duplications..
  */

trait Functions {
  def sum(a: List[Double]): Double

  def concat(a: Seq[String]): String

  def max(a: List[Int]): Int // gives Int.MinValue if a is empty
}

trait Combiner[A] {
  def unit: A

  def combine(a: A, b: A): A
}

object Combiner {
  def apply[A](unit: A, combine: (A, A) => A): Combiner[A] = CombinerImpl(unit, combine)

  private case class CombinerImpl[A](override val unit: A,
                                     comb: (A, A) => A) extends Combiner[A] {

    override def combine(a: A, b: A): A = comb(a, b)
  }

}

object FunctionsImpl extends Functions {

  private def combine[A](a: Seq[A], combiner: Combiner[A]): A = {
    var res: A = combiner.unit
    a.foreach(e => {
      res = combiner.combine(res, e)
    })
    res
  }

  override def sum(a: List[Double]): Double = {
    val f: (Double, Double) => Double = _ + _
    // combine(a, Combiner[Double](0, f))
    combine(a, Combiner(0.0, f))
  }

  override def concat(a: Seq[String]): String = {
    val f: (String, String) => String = _ + _
    combine(a, Combiner("", f))
  }

  override def max(a: List[Int]): Int = {
    /*val f: ( Int , Int ) => Int = (x: Int, y: Int) => (x,y) match {
      case x > y => x
      case _ => y
    }*/
    def f(x: Int, y: Int): Int = {
      if (x >= y) x
      else y
    }

    combine(a, Combiner(Int.MinValue, f))
  }
}

object TryFunctions extends App {
  val f: Functions = FunctionsImpl
  println(f.sum(List(10.0, 20.0, 30.1))) // 60.1
  println(f.sum(List())) // 0.0
  println(f.concat(Seq("a", "b", "c"))) // abc
  println(f.concat(Seq())) // ""
  println(f.max(List(-10, 3, -5, 0))) // 3
  println(f.max(List())) // -2147483648
}