package u05lab.solution

object TicTacToe extends App {

  sealed trait Player {
    def other: Player = this match {
      case X => O;
      case _ => X
    }

    override def toString: String = this match {
      case X => "X";
      case _ => "O"
    }
  }

  case object X extends Player

  case object O extends Player

  case class Mark(x: Double, y: Double, player: Player)

  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Double, y: Double): Option[Player] = board match {
    case Mark(`x`, `y`, p) :: _ => Some(p) // case Mark(a, b, p) :: _ if a == x && b == y => Some(p)
    case _ :: t => find(t, x, y)
    case _ => None
  }

  def placeAnyMark(board: Board, player: Player): Seq[Board] = {
    var everyCombination: Seq[Board] = Seq.empty[Board]
    for (
      x <- 0 until 3;
      y <- 0 until 3
      if find(board, x, y) isEmpty
    ) yield {
      var boardCopy: List[Mark] = board
      boardCopy = boardCopy :+ Mark(x, y, player)
      everyCombination = everyCombination :+ boardCopy
    }
    everyCombination
  }

  def next(player: Player): Player = player match {
    case X => O
    case O => X
  }

  def samePlayer(a: Player, b: Player, c: Player): Boolean = a == b && a == c

  def checkExistanceAndCompare(x: Option[Player], y: Option[Player], z: Option[Player]): Boolean = (x, y, z) match {
    case _ if x.nonEmpty && y.nonEmpty && z.nonEmpty && samePlayer(x.get, y.get, z.get) => true
    case _ => false
  }

  def horizontalWin(board: Board, iteration: Int = 0): Boolean = iteration match {
    case i if i < 3 =>
      val a = find(board, 0, i)
      val b = find(board, 1, i)
      val c = find(board, 2, i)
      if (checkExistanceAndCompare(a, b, c)) {
        true
      } else {
        horizontalWin(board, i + 1)
      }
    case _ => false
  }

  def verticalWin(board: Board, iteration: Int = 0): Boolean = iteration match {
    case i if i < 3 =>
      val a = find(board, i, 0)
      val b = find(board, i, 1)
      val c = find(board, i, 2)
      if (checkExistanceAndCompare(a, b, c)) {
        true
      } else {
        verticalWin(board, i + 1)
      }
    case _ => false
  }

  def diagonalWin(board: Board): Boolean = {
    val a1 = find(board, 0, 0)
    val c1 = find(board, 2, 2)

    val a2 = find(board, 2, 0)
    val c2 = find(board, 0, 2)

    val b = find(board, 1, 1)

    checkExistanceAndCompare(a1, b, c1) || checkExistanceAndCompare(a2, b, c2)
  }

  def gameWon(board: Board): Boolean = {
    horizontalWin(board) || verticalWin(board) || diagonalWin(board)
  }

  def computeAnyGame(player: Player, moves: Int): Stream[Game] = moves match {
    case 0 =>
      var games: List[Game] = List()
      val startingBoard: Board = List()
      val emptyGame: Game = List(startingBoard)
      games = games :+ emptyGame
      games.toStream
    case m =>
      var games: List[Game] = List()
      for (
        game <- computeAnyGame(player, moves - 1);
        possibleMove <- placeAnyMark(game.head, if (m % 2 == 0) next(player) else player)
      ) {
        var gameWithNewMove: Game = game
        if (!gameWon(game.head)) {
          gameWithNewMove = possibleMove +: gameWithNewMove
        }
        games = games :+ gameWithNewMove
      }
      games.toStream
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) {
        print(" ")
        if (board == game.head) println()
      }
    }

  def printBoard(board: Board): Unit =
    for (y <- 0 to 2; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) {
        println()
      }
    }

  // Exercise 1: implement find such that..
  println("Exercise 1")
  println(find(List(Mark(0, 0, X)), 0, 0)) // Some(X)
  println(find(List(Mark(0, 0, X), Mark(0, 1, O), Mark(0, 2, X)), 0, 1)) // Some(O)
  println(find(List(Mark(0, 0, X), Mark(0, 1, O), Mark(0, 2, X)), 1, 1)) // None

  // Exercise 2: implement placeAnyMark such that..
  println("\nExercise 2")
  printBoards(placeAnyMark(List(), X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  println()
  printBoards(placeAnyMark(List(Mark(0, 0, O)), X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  println("\nExercise 3")
  computeAnyGame(O, 6) foreach { g => printBoards(g); println() }
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
}