package u04lab.solution

trait Student {
  def name: String

  def year: Int

  def enrolling(course: Course): Unit // the student participates to a Course
  def enrolling(courseArray: Course*): Unit // the student participates to a Course
  def courses: List[String] // names of course the student participates to
  def hasTeacher(teacher: String): Boolean // is the student participating to a course of this teacher?
}

trait Course {
  def name: String

  def teacher: String
}

object Student {
  def apply(name: String, year: Int = 2017): Student = StudentImpl(name, year)

  private case class StudentImpl(override val name: String,
                                 override val year: Int) extends Student {

    private var coursesList: List[Course] = List()

    override def enrolling(course: Course): Unit = {
      coursesList = List(course) append coursesList
    }

    override def enrolling(courseArray: Course*): Unit = {
      courseArray.foreach(course => coursesList = List(course) append coursesList)
    }

    override def courses: List[String] = {
      val getCourseName: Course => String = x => x.name
      coursesList.map(getCourseName)
    }

    override def hasTeacher(teacher: String): Boolean = {
      val getCourseTeacher: Course => String = x => x.teacher
      coursesList.map(getCourseTeacher).filter(t => t == teacher) match {
        case _ :: _ => true
        case _ => false
      }
      /*coursesList.map(getCourseTeacher).filter(t => t == teacher).head match {
        case Some(_) => true
        case _ => false
      }*/
    }
  }

}

object Course {
  def apply(name: String, teacher: String): Course = CourseImpl(name, teacher)

  private case class CourseImpl(name: String, teacher: String) extends Course

}

object Try extends App {
  val cPPS = Course("PPS", "Viroli")
  val cPCD = Course("PCD", "Ricci")
  val cSDR = Course("SDR", "D'Angelo")
  val s1 = Student("mario", 2015)
  val s2 = Student("gino", 2016)
  val s3 = Student("rino") //defaults to 2017
  s1.enrolling(cPPS)
  s1.enrolling(cPCD)
  s2.enrolling(cPPS)
  s3.enrolling(cPPS)
  s3.enrolling(cPCD)
  s3.enrolling(cSDR)
  println(s1.courses, s2.courses, s3.courses) // (Cons(PCD,Cons(PPS,Nil())),Cons(PPS,Nil()),Cons(SDR,Cons(PCD,Cons(PPS,Nil()))))
  println(s1.hasTeacher(cPCD.teacher)) // true
  println(s1.hasTeacher(cSDR.teacher)) // false
  val s4 = Student("luigi")
  s4.enrolling(cPPS, cPCD)
  println(s4.courses) // Cons(PCD,Cons(PPS,Nil()))
}

/** Hints:
  * - simply implement Course, e.g. with a case class
  * - implement Student with a StudentImpl keeping a private Set of courses
  * - try to implement in StudentImpl method courses with map
  * - try to implement in StudentImpl method hasTeacher with map and find
  * - check that the two println above work correctly
  * - refactor the code so that method enrolling accepts a variable argument Course*
  */
